// for client side we use IIFE to execute the application


(function() {
    // Everything inside this function is our application

    // Create an instance of Angular application/module
    // 1st param: app's name, the is the apps dependencies
    var FirstApp = angular.module("FirstApp", []);

    var FirstCtrl = function() {
        // Hold a reference of this controller so that when this changes we are 
        // still referencing the controller
        var firstCtrl = this;

        

    }

    // Define a controller call FirstCtrl -
    FirstApp.controller("FirstCtrl", [FirstCtrl]);

})();
