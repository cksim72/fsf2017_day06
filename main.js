// JS template

// Load express and path
var express = require("express");
var path = require("path");

// Create instance of express app
var app = express();


// Define routes
app.use(express.static(path.join(__dirname,"public")));
// appExpress.use("/images", express.static(path.join(__dirname,"public")); // Remap

app.use("/libs", express.static(path.join(__dirname,"bower_components")));

// Catch-all function
app.use(function(req,resp) {
    resp.status(404);
    resp.type("text/html"); // Representation of resource
    resp.send("Resource not available");
});


// Set PORT - default to 3000 if no argument found
app.set("port",parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

// Listen to PORT
app.listen(app.get("port"), function() {
    console.info("Application is listening on port " + app.get("port"));
});

